plugins {
    kotlin("jvm")
}

group = "net.besttoolbars"
version = "0.0.1-SNAPSHOT"

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    implementation("net.besttoolbars:awin-connector:1.1.0")
    implementation("net.besttoolbars:cj-connector:1.0.1")
    implementation("net.besttoolbars:dcm-connector:1.0.3")
    implementation("net.besttoolbars:impact-radius-connector:1.0.0")
    implementation("net.besttoolbars:lomadee-connector:1.0.0")
    implementation("net.besttoolbars:rakuten-connector:1.0.2")
    implementation("net.besttoolbars:tradedoubler-connector:1.0.0")
}
