package net.besttoolbars.connectors.affiliates.cj

data class CjCredentials(
    val requestorId: String,
    val websiteId: String,
    val extPromotionId: String,
    val accessToken: String
)