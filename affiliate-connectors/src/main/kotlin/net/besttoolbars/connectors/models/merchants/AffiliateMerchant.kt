package net.besttoolbars.connectors.models.merchants

import net.besttoolbars.connectors.models.Commission
import net.besttoolbars.connectors.models.common.ActivationLink
import net.besttoolbars.connectors.models.common.AffiliateEnum
import net.besttoolbars.connectors.models.common.CommonCategory
import net.besttoolbars.connectors.models.common.Language

data class AffiliateMerchant(
    val extId: String,
    val affiliate: AffiliateEnum,
    val activationLinks: List<ActivationLink> = emptyList(),
    val domain: String,
    val originalDomain: String?,
    val categories: Set<CommonCategory> = emptySet(),
    val originalCategories: Set<String> = emptySet(),
    val name: String,
    val image: String? = null,
    val languages: Set<Language>,
    val regions: Set<String> = emptySet(),
    val sevenDayEpc: Float? = null,
    val threeMonthEpc: Float? = null,
    val description: String? = null,
    val commissions: List<Commission>? = null,
    val attribution: AttributionType? = null
)
