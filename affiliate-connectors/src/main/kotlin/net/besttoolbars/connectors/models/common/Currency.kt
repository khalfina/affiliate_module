package net.besttoolbars.connectors.models.common

enum class Currency {
    USD,
    AED
}