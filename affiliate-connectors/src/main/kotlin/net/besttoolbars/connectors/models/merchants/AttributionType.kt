package net.besttoolbars.connectors.models.merchants

enum class AttributionType {
    COUPON,
    CASHBACK,
    BOTH,
    NONE
}