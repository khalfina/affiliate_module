package net.besttoolbars.connectors.models.offers

import net.besttoolbars.connectors.models.Commission
import net.besttoolbars.connectors.models.common.ActivationLink
import net.besttoolbars.connectors.models.common.AffiliateEnum
import net.besttoolbars.connectors.models.common.CommonCategory
import net.besttoolbars.connectors.models.common.Language
import java.time.LocalDateTime

data class AffiliateOffer(
    val id: String,
    val extId: String?,
    val merchantId: String,
    val merchantExtId: String?,
    val affiliate: AffiliateEnum,
    val startDate: LocalDateTime? = null,
    val endDate: LocalDateTime? = null,
    val image: String?,
    val localizedName: Map<Language, String> = emptyMap(),
    val localizedDescription: Map<Language, String> = emptyMap(),
    val categories: Set<CommonCategory>,
    val originalCategories: Set<String>,
    val languages: Set<Language>,
    val commissions: List<Commission>,
    val domain: String,
    val originalDomain: String,
    val regions: Set<String> = emptySet(),
    val type: OfferType,
    val promoCodes: Set<String>,
    val activationLinks: List<ActivationLink>,
    val iframe: Iframe? = null
) {
    data class Iframe(val src: String? = null, val height: Int? = null, val width: Int? = null)
}