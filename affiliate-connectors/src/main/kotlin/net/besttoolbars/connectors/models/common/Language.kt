package net.besttoolbars.connectors.models.common

enum class Language {
    EN,
    CS,
    DA,
    NL,
    FR,
    DE,
    IT,
    PL,
    PT,
    RU,
    ES,
    SV,
    AR,
    BN,
    BG,
    FI,
    EL,
    HI,
    HU,
    JP,
    KO,
    NO,
    RO,
    SL,
    TR,
    VI,
    UNKNOWN;


    fun isUnknown() = this == UNKNOWN

    companion object {
        fun fromString(value: String): Language {
            return try {
                valueOf(value.toUpperCase())
            } catch (e: Exception) {
                UNKNOWN
            }
        }
    }
}
