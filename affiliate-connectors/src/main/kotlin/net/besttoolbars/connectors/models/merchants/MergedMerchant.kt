package net.besttoolbars.connectors.models.merchants

import net.besttoolbars.connectors.models.common.ActivationLink
import net.besttoolbars.connectors.models.common.CommonCategory
import net.besttoolbars.connectors.models.common.Language

data class MergedMerchant(
    val id: String,
    val domain: String,
    val image: String? = null,
    val categories: Set<CommonCategory> = emptySet(),
    val activationLinks: List<ActivationLink> = emptyList(),
    val manual: Boolean = false,
    val affiliateMerchants: List<AffiliateMerchant> = emptyList(),
    val localizedName: Map<Language, String> = emptyMap(),
    val localizedDescription: Map<Language, String> = emptyMap(),
    val languages: Set<Language> = emptySet()
)