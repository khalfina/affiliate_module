package net.besttoolbars.connectors.models.common

data class ActivationLink(
    val affiliate: AffiliateEnum,
    val activationLink: String,
    val linkType: LinkType = LinkType.GENERIC
) {
    enum class LinkType {
        GENERIC,
        WEBSITE,
        EXTENSION,
        MOBILEAPP;

        companion object {
            fun fromString(value: String): LinkType? {
                return try {
                    valueOf(value.toUpperCase())
                } catch (e: Exception) {
                    null
                }
            }
        }
    }
}

fun Collection<ActivationLink>.getByType(linkType: ActivationLink.LinkType? = null): String? {
    val activationLink = find { it.linkType == linkType }
        ?: find { it.linkType == ActivationLink.LinkType.WEBSITE }
        ?: find { it.linkType == ActivationLink.LinkType.EXTENSION }
        ?: find { it.linkType == ActivationLink.LinkType.MOBILEAPP }
        ?: find { it.linkType == ActivationLink.LinkType.GENERIC }
    return activationLink?.activationLink
}
