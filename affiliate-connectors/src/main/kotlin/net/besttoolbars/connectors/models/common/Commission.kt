package net.besttoolbars.connectors.models

import net.besttoolbars.connectors.models.common.Currency

sealed class Commission(
    var type: CommissionType
)

class FixedCommission(
    val amount: Double,
    val currency: Currency
) : Commission(CommissionType.FIXED)

class FixedPerOrderCommission(
    val amount: Double,
    val currency: Currency
) : Commission(CommissionType.FIXED_PER_ORDER)

class PercentageCommission(
    val percent: Double
) : Commission(CommissionType.PERCENT)

enum class CommissionType {
    FIXED,
    PERCENT,
    FIXED_PER_ORDER
}