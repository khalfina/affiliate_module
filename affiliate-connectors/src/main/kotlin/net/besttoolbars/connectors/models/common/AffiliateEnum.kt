package net.besttoolbars.connectors.models.common

enum class AffiliateEnum {
    AWIN, // в каком проекте
    CJ,
    DCM,
    RADIUS,
    LOMADEE,// в каком проекте
    RAKUTEN,
    GDELSON,// в каком проекте
    ADMITAD, // в каком проекте
    EBAY, // в каком проекте
    AMAZON, // мы с ним интегрялись?
    CUSTOM
}