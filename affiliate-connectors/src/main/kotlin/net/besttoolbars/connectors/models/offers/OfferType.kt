package net.besttoolbars.connectors.models.offers

enum class OfferType {
    PRODUCT,
    COUPON,
    DEAL,
    BANNER,
    BANNER_IFRAME,
    FREE_SHIPPING,
    UNKNOWN
}