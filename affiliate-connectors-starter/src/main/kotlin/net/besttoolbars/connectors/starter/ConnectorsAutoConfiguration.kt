package net.besttoolbars.connectors.starter

import net.besttoolbars.connectors.service.FeedService
import net.besttoolbars.connectors.service.FeedServiceImpl
import net.besttoolbars.connectors.starter.ConnectorsAutoConfiguration.Companion.CONNECTORS_CONFIG_PREFIX
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean

@ConditionalOnClass(FeedServiceImpl::class)
@ConditionalOnProperty(prefix = CONNECTORS_CONFIG_PREFIX, name = ["enabled"], havingValue = "true")
@EnableConfigurationProperties(ConnectorStarterProperties::class)
class ConnectorsAutoConfiguration(
    private val properties: ConnectorStarterProperties
) {
    @Bean
    @ConditionalOnMissingBean(FeedService::class)
    fun getConnectorsService(): FeedService {
        return FeedServiceImpl() // todo
    }

    companion object {
        const val CONNECTORS_CONFIG_PREFIX = "bestoolbars.connectors"
    }
}