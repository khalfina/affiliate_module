package net.besttoolbars.connectors.starter

import net.besttoolbars.connectors.affiliates.cj.CjCredentials
import net.besttoolbars.connectors.starter.ConnectorsAutoConfiguration.Companion.CONNECTORS_CONFIG_PREFIX
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(CONNECTORS_CONFIG_PREFIX)
data class ConnectorStarterProperties(
    val enabled: Boolean,
    val cj: CjCredentials?
// todo
)